STOW_PACKAGES=readline
TARGET=${HOME}

OH_MY_ZSH_PATH="$(TARGET)/.oh-my-zsh"
SCM_BREEZE_PATH="$(TARGET)/.scm_breeze"


default:
	@echo "==> Nothing to be done, use 'make install'"


install: stow nvim_install zsh_install

clean: nvim_clean zsh_clean unstow


nvim_install:
	stow --restow --target=$(TARGET) nvim
	git clone --depth 1 https://github.com/wbthomason/packer.nvim\
		 ~/.local/share/nvim/site/pack/packer/start/packer.nvim
	nvim -u ~/.config/nvim/lua/plugins.lua --headless -c 'autocmd User PackerComplete quitall' -c 'PackerSync'

nvim_clean:
	rm -rf ~/.local/share/nvim
	rm -rf ~/.config/nvim/plugin
	rm -rf ./nvim/.config/nvim/plugin
	stow --delete --target=$(TARGET) nvim


oh_my_zsh_install:
	@echo "==> install oh-my-zsh"
	git clone https://github.com/robbyrussell/oh-my-zsh.git $(OH_MY_ZSH_PATH)

oh_my_zsh_clean:
	rm -rf $(OH_MY_ZSH_PATH)


# FIXME: scm_breeze is a dirty hack and should be burned with the sacred fire of doom remove it as soon as possible
scm_breeze_install:
	@echo "==> install scm_breeze"
	git clone https://github.com/scmbreeze/scm_breeze.git $(SCM_BREEZE_PATH)
	"$(SCM_BREEZE_PATH)/install.sh"
	# FIXME: massive hack to restore zshrc
	git checkout ./zsh/.zshrc

scm_breeze_clean:
	rm -rf $(SCM_BREEZE_PATH)


zsh_install: oh_my_zsh_install scm_breeze_install
	stow --target=$(TARGET) zsh


zsh_clean: oh_my_zsh_clean scm_breeze_clean
	stow --delete --target=$(TARGET) zsh

stow:
	@echo "==> linking dotfiles (stowing)"
	stow --restow --target=$(TARGET) $(STOW_PACKAGES)

unstow:
	@echo "==> removing dotfile links (unstowing)"
	stow --delete --target=$(TARGET) $(STOW_PACKAGES)
