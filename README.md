# Installation

```bash
$ git clone git@gitlab.com:vise890/dotfiles.git
$ cd dotfiles
$ make install
```

If you want to install the extra configs (e.g. `./hidpi`):
```
stow -t ~ hidpi
```

## Pre-requisites
- `git`
- GNU `stow`
- GNU `make`
