# NOTE: If you see values, do not edit directly
#
# Uses Python 3 template
# https://docs.python.org/3.6/library/string.html#template-strings
source ~/.config/mutt/colors/mutt-colors-solarized-dark-16.muttrc

set realname = "$user_full_name"
set from = "$user_email"
set use_from

set mbox_type = Maildir
set sendmail = "msmtp"

set folder = $email_maildir_path
set header_cache = ~/.cache/mutt
# folder where your (unfiltered) e-mail arrives
set spoolfile = +INBOX
set record = +sent
set postponed = +drafts

# Allow Mutt to open new imap connection automatically.
unset imap_passive
set imap_user = "$user_email"
set imap_pass = "$user_email"
set mail_check = 5 # seconds
set timeout = 10
set imap_keepalive = 120

set smtp_url = smtps://${email_username}:${email_password}@$email_smtp_hostname
set ssl_force_tls


# Editor
set editor='nvim +/^$$ "+setlocal fo+=w spell" +noh'
set text_flowed
set tmpdir="~/"

# Message formatting
alternative_order text/plain text/html
auto_view text/html
set pipe_decode
set print_decode
set print_command='muttprint -P letter -F Helvetica -p TO_FILE:$$HOME/mail.ps'

# Outgoing
set smtp_url="smtp://localhost:26"


# Index interface
bind index g noop

unset mark_old
set thorough_search
set quit
set menu_scroll
set include
set fast_reply
bind index G last-entry
bind index gg first-entry
bind index R group-reply
bind index <space> collapse-thread
bind index V collapse-all
bind index \cU half-up
bind index \cD half-down
bind index \cB previous-page
bind index \cF next-page
macro index d "<save-message>=trash<enter>"
macro index a "<limit>~A<enter>"
macro index i "<limit>~U | ~F<enter>"
macro index I "<limit>~U<enter>"

set sort="threads"
set sort_aux="reverse-last-date-received"
set strict_threads
set narrow_tree
unset confirmappend
unset collapse_unread
unset help
set delete="yes"

# mu search
unset wait_key
macro index S "<save-message>"
macro index s "<shell-escape>mufind "
macro index gs "<change-folder-readonly>~/.search<enter>"

# Index style
set date_format="%Y-%m-%d"
set index_format="%D %Z %-15.15L %s"

# Pager interface
bind pager g noop

set pager_stop
unset markers
bind pager gg top
bind pager G bottom
bind pager R group-reply
bind pager j next-line
bind pager k previous-line
bind pager \Cu half-up
bind pager \Cd half-down
bind pager \Cf next-page
bind pager \Cb previous-page
bind pager <space> next-page
bind pager <backspace> previous-page
macro pager d "<save-message>=trash<enter>"

# Editor interface
bind editor <space> noop
