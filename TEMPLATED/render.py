#!/usr/bin/env python3
"""
Renders $1.templates to $1.out

Uses string.Template with variables from "variables.json"
to populate placeholders.
"""

import string
import json
import os
import shutil
import sys


def render_file(path, template_str, variables):
    with open(path, "w") as dest:
        t = string.Template(template_str)
        contents = t.substitute(variables)
        dest.write(contents)


def json_loadf(path):
    """Read path as JSON."""
    with open(path, "r") as vf:
        return json.load(vf)


def render(pkg_name):
    """
    Render all the files in "{pkg_name}.templates" to "{pkg_name}.out".

    Uses variables from "{pkg_name}.variables.json"
    """
    in_path = os.path.join(f"{pkg_name}.templates", "")
    out_path = os.path.join(f"{pkg_name}.out", "")
    variables_path = f"{pkg_name}.variables.json"

    shutil.rmtree(out_path, ignore_errors=True)
    shutil.copytree(in_path, out_path)

    variables = json_loadf(variables_path)

    for (dirpath, _, filenames) in os.walk(out_path):

        for fn in filenames:
            fp = os.path.join(dirpath, fn)

            with open(fp, "r") as f:
                contents = f.read()

            render_file(fp, contents, variables)


if __name__ == "__main__":
    render(sys.argv[1])
