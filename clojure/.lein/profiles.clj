{:user {:plugins      [[cider/cider-nrepl "0.28.5"]
                       [com.billpiel/sayid "0.1.0"]
                       [jonase/eastwood "1.2.5"]
                       [lein-cloverage "1.2.4"]
                       [lein-eftest "0.5.9"]
                       [lein-pprint "1.3.2"]
                       [refactor-nrepl "3.5.5"]

                       ; [mvxcvi/puget "1.1.1"]
                       ; [venantius/ultra "0.6.0" :exclusions [mvxcvi/puget]]

                       ]

        :dependencies [[alembic "0.3.2" :exclusions [org.tcrawley/dynapath]]
                       [nrepl "1.0.0"]]}

 :misc {:plugins [[lein-marginalia "0.9.1"]
                  [lein-hiera "1.1.0"]
                  [lein-ancient "0.7.0"]]}

 :lint {:plugins      [[jonase/eastwood "1.2.5"]
                       [lein-kibit "0.1.8"]
                       [lein-bikeshed "0.5.2"]
                       [venantius/yagni "0.1.7"]]
        :dependencies [[slamhound "1.5.5"]]
        :aliases      {"slamhound" ["run" "-m" "slam.hound"]}}

 :fmt  {:plugins [[lein-cljfmt "0.9.0"]
                  [lein-zprint "1.2.4.1"]]
        :zprint  {:old? false}}}
