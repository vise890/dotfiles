export QT_QPA_PLATFORMTHEME="qt6ct"

# https://wiki.archlinux.org/title/HiDPI#Qt_5
# this sort of works with predator for kde apps (e.g., dolphin, kalendar, okular)
# BUT: it breaks with other KDE infra (e.g., system settings)
# export QT_SCALE_FACTOR=0.8

# https://wiki.archlinux.org/title/Dolphin#Icons_are_too_big
# export XDG_CURRENT_DESKTOP=KDE
# export KDE_SESSION_VERSION=5
export QT_AUTO_SCREEN_SCALE_FACTOR=0

# XDG
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"
