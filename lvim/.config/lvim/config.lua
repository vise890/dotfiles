-- UI
lvim.background = "light"
lvim.colorscheme = "everforest"

-- Keys
vim.g.maplocalleader = ","
lvim.keys.normal_mode["Q"] = "gqip$"
lvim.keys.insert_mode["jk"] = "<ESC>"
lvim.keys.insert_mode["kj"] = "<ESC>"

-- Additional Plugins
-- discover some here : https://www.lunarvim.org/plugins/02-extra-plugins.html
lvim.plugins = {
  { "sainnhe/everforest" },
  { "NoahTheDuke/vim-just" },
  { 'freitass/todo.txt-vim' },
}
