-- TODO: todo highlight
-- TODO: completion from lsp
-- TODO: automatic lsp download
-- TODO: fix todotxt

-- local vimp = require('vimp')

-- vim.cmd('setlocal spell spelllang=en_us')

vim.g.mapleader = ' '
vim.g.maplocalleader = ','

vim.opt.mouse = 'a'                         -- still be able to use mouse
vim.opt.clipboard:append({'unnamedplus'})   -- use OS clipboard
vim.opt.autowrite = true

-- Key Mappings
local function map(mode, lhs, rhs)
  vim.api.nvim_set_keymap(mode, lhs, rhs, {noremap=true})
end
-- https://github.com/nanotee/nvim-lua-guide#defining-mappings
map('i', 'jk', '<Esc>')
map('i', 'JK', '<Esc>')
map('', '<F1>', '<Nop>')
map('', '<F1>', '<Nop>')
-- move in a sane way when wrapping
map('n', 'k', 'gk')
map('n', 'j', 'gj')
map('n', '0', 'g0')
map('n', '$', 'g$')
-- shortcut for to wrap all lines at 80 characters without breaking any words,
-- and preserve shorter lines:
map('n', 'Q', 'gqip$')
-- yank from cursor to end of line (more consistent with A, C, D, etc.)
map('n', 'Y', 'y$')
-- move around
map('n', '<C-h>', '<C-w>h')
map('n', '<C-j>', '<C-w>j')
map('n', '<C-k>', '<C-w>k')
map('n', '<C-l>', '<C-w>l')

map('n', '<leader>s', ':PackerSync<CR>')

map('n', '<C-n>', ':NvimTreeToggle<CR>')
local telescope = require('telescope.builtin')
map('n', '<leader>ff', ':Telescope find_files<CR>')
map('n', '<C-p>', ':Telescope find_files<CR>')
map('n', '<leader>fg', ':Telescope live_grep<CR>')
map('n', '<leader>fb', ':Telescope buffers<CR>')
map('n', '<leader>fh', ':Telescope help_tags<CR>')

-- UI
vim.opt.cursorline = true
vim.opt.number = true
vim.opt.scrolloff = 5
vim.opt.showmatch = true
vim.opt.termguicolors = true

vim.opt.background = 'light'
vim.cmd('colorscheme everforest')

-- Highlight some types of whitespace
vim.opt.list = true
vim.opt.listchars = {tab = '› ', nbsp = '·'}

-- search
vim.opt.hlsearch = true
vim.opt.ignorecase = true
vim.opt.incsearch = true
vim.opt.infercase = true
vim.opt.smartcase = true

-- Ignore all the crap
vim.opt.wildignore:append({'*/tmp/*', '*.so', '*.swp', '*.zip', '*/node_modules/*'})
