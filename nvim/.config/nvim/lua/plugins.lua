require('packer').startup(function(use)

  use 'wbthomason/packer.nvim'
  use 'svermeulen/vimpeccable'

  -- Themes
  -- use 'shaunsingh/solarized.nvim'
  -- use 'savq/melange'
  use {"sainnhe/everforest"}

  -- LSP, Treesitter
  use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
  use { 'neovim/nvim-lspconfig' }
  use { 'alexaandru/nvim-lspupdate' }
  use {'mfussenegger/nvim-lint'}

  -- Editing
  use 'tpope/vim-commentary'
  use 'tpope/vim-surround' -- fiddle with surrounding quotes/parens/tags/etc.
  use 'jiangmiao/auto-pairs'
  use 'bronson/vim-trailing-whitespace'

  -- UX
  use {
    'folke/which-key.nvim',
    config = function()
      require('which-key').setup {}
    end
  }
  use {
    'kyazdani42/nvim-tree.lua',
    requires = {
      'kyazdani42/nvim-web-devicons', -- optional, for file icon
    },
    config = function()
      vim.opt.termguicolors = true
      require('nvim-tree').setup {}
    end
  }
  use {
    'nvim-telescope/telescope.nvim',
    requires = { {'nvim-lua/plenary.nvim'} },
  }
  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons', opt = true },
    config = function()
      require('lualine').setup()
    end
  }

  -- Completion
  use {
    'hrsh7th/nvim-cmp',
    requires = {
      'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-buffer',
      'hrsh7th/cmp-path',
      'hrsh7th/cmp-cmdline',
      -- 'hrsh7th/cmp-vsnip',
      'hrsh7th/vim-vsnip',
    },
    -- https://github.com/hrsh7th/nvim-cmp#recommended-configuration
    config = function()
      vim.opt.completeopt = { 'menu', 'menuone', 'noselect'}
      local cmp = require('cmp')
      cmp.setup({
        snippet = { expand = function(args) vim.fn['vsnip#anonymous'](args.body) end },
        mapping = {
          ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
          ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
          ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
          -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
          ['<C-y>'] = cmp.config.disable,
          ['<C-e>'] = cmp.mapping({
            i = cmp.mapping.abort(),
            c = cmp.mapping.close(),
          }),
          -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
          ['<CR>'] = cmp.mapping.confirm({ select = true }),
          ['<Tab>'] = cmp.mapping.confirm({ select = true }),
        },
        sources = cmp.config.sources({
          { name = 'nvim_lsp' },
          { name = 'vsnip' },
        }, {
          { name = 'buffer' },
        })
      })
    end
  }

  -- language support
  use { 'tpope/vim-sleuth' } -- heuristically set buffer options (shiftwidth, expandtab, etc.)

  -- use { 'Olical/eonjure' }
  -- use { 'rktjmp/hotpot.nvim' }
  -- use { 'davidhalter/jedi-vim' , ft = 'python' }
  use { 'ekalinin/Dockerfile.vim' }
  use { 'freitass/todo.txt-vim' }
  -- use { 'jceb/vim-orgmode', ft = 'org' }
  -- use { 'nvie/vim-flake8', ft = 'python' }
  -- use { 'rust-lang/rust.vim', ft = 'rust' }
  -- use { 'sheerun/vim-polyglot' }

  use { 'tjdevries/nlua.nvim', ft = 'lua' }
  use { 'nvim-lua/completion-nvim', ft = 'lua' }
  use { 'euclidianAce/BetterLua.vim', ft = 'lua' }
  use { 'tjdevries/manillua.nvim', ft = 'lua' }

end)
