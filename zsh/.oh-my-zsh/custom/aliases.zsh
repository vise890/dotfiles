alias vim="$EDITOR"

alias hx="helix"

alias j="z"

zshrc="$HOME/.zshrc"
function zrl { source "$zshrc" }
alias zshrc="$EDITOR $zshrc && zrl"
alias zaliases="$EDITOR $HOME/.oh-my-zsh/custom/aliases.zsh && zrl"
alias zprofile="$EDITOR $HOME/.zprofile && zrl"


alias bp='bpython'

alias ee='espanso edit'

alias y='yazi'
