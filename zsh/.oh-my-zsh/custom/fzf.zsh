_gen_fzf_default_opts() {
  local base03="8"
  local base02="0"
  local base01="10"
  local base00="11"
  local base0="12"
  local base1="14"
  local base2="7"
  local base3="15"
  local yellow="3"
  local orange="9"
  local red="1"
  local magenta="5"
  local violet="12"
  local blue="4"
  local cyan="6"
  local green="2"

  # Options
  #  fg Text
  #  bg Background
  #  hl Highlighted substrings
  #  fg+ Text (current line)
  #  bg+ Background (current line)
  #  hl+ Highlighted substrings (current line)
  #  info Info
  #  prompt Prompt
  #  pointer Pointer to the current line
  #  marker Multi-select marker
  #  spinner Streaming input indicator
  #  header Header
  #
  FZF_DEFAULT_OPTS="--color hl:$yellow,fg+:$base03,bg+:$base01,hl+:$base2"
  FZF_DEFAULT_OPTS+=" --color info:$base01,prompt:$base01,pointer:$base03,marker:$base03,spinner:$base01"
  export FZF_DEFAULT_OPTS
}
_gen_fzf_default_opts

# Use custom trigger sequence instead of the default **
export FZF_COMPLETION_TRIGGER=',,'

# Options to fzf command
export FZF_COMPLETION_OPTS='--extended'

# Use fd (https://github.com/sharkdp/fd) instead of the default find
# command for listing path candidates.
# - The first argument to the function ($1) is the base path to start traversal
# - See the source code (completion.{bash,zsh}) for the details.
_fzf_compgen_path() {
  fd --hidden --follow --exclude ".git" . "$1"
}

# Use fd to generate the list for directory completion
_fzf_compgen_dir() {
  fd --type d --hidden --follow --exclude ".git" . "$1"
}

bindkey '^R' fzf-history-widget
bindkey '^F' fzf-history-widget
