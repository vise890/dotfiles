# scripts in $CODEPATH/scripts

# TODO: see if these can be moved in ../completions
_scripts_names() {
    local matches=(`ls $CODEPATH/scripts`)
    compadd -a matches
}
compdef _scripts_names script_new
compdef _scripts_names ns

compdef _scripts_names whichcat
compdef _scripts_names catwhich
compdef _scripts_names whichbat
compdef _scripts_names batwhich

# run a script in "$CODEPATH/scripts"
#
# adapted from fzf-cd-widget
fzf-run-script-widget() {

  local scripts_path="$CODEPATH/scripts"

  local cmd="find $scripts_path -type f -executable -print0 2> /dev/null | sed --null-data s:$scripts_path/::"

  setopt localoptions pipefail no_aliases 2> /dev/null

  local script="$(eval "$cmd" | FZF_DEFAULT_OPTS="--height ${FZF_TMUX_HEIGHT:-40%} --reverse --bind=ctrl-z:ignore $FZF_DEFAULT_OPTS $FZF_ALT_C_OPTS" $(__fzfcmd) --read0 +m)"

  if [[ -z "$script" ]]; then
    zle redisplay
    return 0
  fi

  zle push-line # Clear buffer. Auto-restored on next prompt.
  BUFFER="${(q)script} "
  local ret=$?
  unset script  # ensure this doesn't end up appearing in prompt expansion

  zle reset-prompt

  # go to end of line in case you want to pass in args
  zle end-of-line

  return $ret
}
zle -N fzf-run-script-widget
bindkey '^K' fzf-run-script-widget
bindkey '^K' fzf-run-script-widget
