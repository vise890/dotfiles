export CODEPATH="$HOME/code"
export TODO_DIR="$HOME/Documents/todo"

# NPM / node stuff
PATH=$HOME/.config/yarn/global/node_modules/.bin:$HOME/.npm/bin:$PATH

# Go's stuff
export GOPATH=$CODEPATH/go
PATH=$GOPATH/bin:$PATH

# rust / cargo
PATH=$HOME/.cargo/bin:$PATH
# export RUST_SRC_PATH="$(rustc --print sysroot)/lib/rustlib/src/rust/src"

# haskell
export PATH="$HOME/.cabal/bin:$HOME/.ghcup/bin:$PATH"

# pip, stack, and lots of others.. 
PATH=$HOME/.local/bin:$PATH

# pyenv
# export PYENV_ROOT="$HOME/.pyenv"
# export PATH="$PYENV_ROOT/bin:$PATH"
# eval "$(pyenv init --path)"

# Doom Emacs
# PATH="$HOME/.emacs.d/bin:$HOME/.doom.emacs.d/bin:$PATH"

# useful user scripts
PATH=$CODEPATH/scripts:$PATH

# user bin dir
PATH=$CODEPATH/bin:$PATH

# Ensure path does not contain duplicates.
typeset -gU path
