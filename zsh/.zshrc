#!/bin/zsh

# Profiling
# Uncomment this line (and `zprof` at the end) to enable profiling
# zmodload zsh/zprof

export ZSH=$HOME/.oh-my-zsh

# TODO: move to .zshenv ?
export EDITOR='lvim'

# $PATH & friends
# FIXME: shouldn't need to do this https://www.zerotohero.dev/zshell-startup-files/
[ -f /etc/profile ] && source /etc/profile
source $HOME/.zprofile

# FZF
export FZF_BASE='/usr/share/fzf'


# plugins can be found in ~/.oh-my-zsh/plugins/
plugins=(

  # direnv
  # nvm
  # pyenv
  # terraform
  # extract
  # rsync

  colored-man-pages
  fzf
  last-working-dir
  mise
  scm_breeze # cost: ~20ms
  ssh-agent
  vi-mode
  zoxide

)

zstyle :omz:plugins:ssh-agent lazy yes

if [ $(whoami) = 'root' ]; then
  export ZSH_THEME="gianu"
else
  export ZSH_THEME="amuse"
fi
export COMPLETION_WAITING_DOTS="true"

setopt correct

DISABLE_AUTO_UPDATE=true
source "$ZSH/oh-my-zsh.sh"

# Syntax highlighting
source \
  /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Profiling
# zprof
